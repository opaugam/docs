***************************************
 Getting started with Platform Features
***************************************

xxxxxx

+-----------------------------------+-----------------------------------+
| Figures                           | Description                       |
+===================================+===================================+
| .. image:: ../img/web-portal.png  |  :ref:`rst_portal`                |
|    :scale: 100 %                  |                                   |
+-----------------------------------+-----------------------------------+
| .. image:: ../img/web-portal.png  |  :ref:`rst_reporting`             |
|    :scale: 100 %                  |                                   |
+-----------------------------------+-----------------------------------+
| .. image:: ../img/web-portal.png  |  :ref:`rst_datacollection`        |
|    :scale: 100 %                  |                                   |
+-----------------------------------+-----------------------------------+
| .. image:: ../img/web-portal.png  |  :ref:`rst_localization`          |
|    :scale: 100 %                  |                                   |
+-----------------------------------+-----------------------------------+

:ref:`rst_portal`

:ref:`rst_reporting`

:ref:`rst_datacollection`

:ref:`rst_localization`




