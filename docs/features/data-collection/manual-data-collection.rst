Manual Data Entry - List and Forms
**********************************

You can accomplish form layout in a graphical tool called Ignimotion Studio. You can also access field properties and add information or scripted UI elements.
You can use the Ignimotion Studio feature to quickly create new or change existing forms.

Users with the appropriate roles can configure various aspects of lists and forms. Configuration changes apply to all users.

With Form configuration, you can add, remove, and reorder list columns. 
You can configure calculations to appear under columns. You can also hide controls and define access conditions by role for existing forms.

.. note:: Configuring a list in this way modifies the list for all users. To make changes to a list that are visible to you only, see Personal lists.

Create a document
=================

see :
:ref:`rst_documentmanagement`

Configure Form layout
=====================

You can configure a form to choose which columns appear in a list, create a document and add a Form Widget, and select the table and the fields to display.

**Procedure**

* Navigate to the document you want to configure.
* Select xx

xxEcran/videoxxx

Configure Form control
======================

xxx


Configure Form fields
=====================

xxx
