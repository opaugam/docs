.. _rst_datacollection:

************************************
Getting started with Data Collection
************************************

.. sidebar:: Related tasks

    .. image:: ../../img/logo.png

    * Creating Data Model
    * Best practices

xx
xx

.. toctree::
    :maxdepth: 1

    manual-data-collection
    automatic-data-collection
