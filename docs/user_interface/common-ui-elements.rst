******************
Common UI elements
******************


Log in to an instance
*********************

Each Ignimotion instance has a unique, secure web address. The base URL for each instance has the default format: https://<instancename>.ignimotion.com.

Before you begin
Role required: none

About this task
Users log in to the instance from a web browser. Administrators can associate a custom URL, such as support.myurl.com, that resolves to point to instancename.ignimotion.com.

Procedure

  Enter the base URL in any web browser.
     If your system uses external authentication, you are automatically logged in. For example, you may log in to company services when you log in to your computer.
      If your system does not use external authentication, the Welcome page appears.
    Enter your user name and password.
    (Optional) Select the Remember Me check box to remain logged in until you manually log out.
    The administrator can enable or disable this option. For more information, see Change settings for the Remember me check box and cookie.
    Press the Enter key or click **Login**. 

Navigation
**********
xxx

* Menu
* Box
* Breadcrumbs


List
****

A list displays a set of records from a table.

Users can search, sort, filter, and edit data in lists. Lists may be embedded in forms and may be hierarchical (have sublists).

The list interface consists of a title bar, filters and breadcrumbs, columns of data, and a footer. Each column in a list corresponds to a field on the table.

Watch the twelve-minute video User Interface | Getting Started with Lists for an in-depth introduction to list functionality.

xxxVideoxxx

Forms
*****

A form displays information from one record in a data table.

The specific information on a form depends on the type of record displayed. Users can view and edit records in forms. Administrators can configure what appears on forms. Watch this ten-minute video for an overview of the features and functions of Ignimotion forms:

xxxVideoxxx

Dashboard
*********
