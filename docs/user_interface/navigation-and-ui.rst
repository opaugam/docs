*********************************
 Getting started with Platform UI
*********************************
.. sidebar:: Related tasks

    .. image:: ../img/logo.png

    * Configure logo
    * Change default colors
    * Change system defaults

**Access data on any device through a configurable web-based interface**

Use the default Ignimotion Platform user interface to access lists and forms, real-time form updates, user presence, an application navigator with tabs for favorites and history, and an activity stream.

As a consumer/contributor, you'll work in the Ignimotion portal to review and interact with content that has been shared with you. 
This module provides the foundational information that you need to work effectively in the Ignimotion Portal.

Watch the five-minute video User Interface | Overview to learn about the elements of the user interface. 

xxxVideoxxx

 .. raw :: html

    <iframe width="700" height="315"
    src="https://www.youtube.com/embed/H6cSXT1mxI8"
    frameborder="0" allowfullscreen></iframe>
   
   
**User Interface**

xxxImagexxx

**Navigation Components**

+-------------------------+----------------------------------------------------------------------+
| Component               | Description                                                          |
+=========================+======================================================================+
| Menu                    | Navigation xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Box                     | Navigation xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Breadcrumbs             | Navigation xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Drill down              | Navigation xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Drill through           | Navigation xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+

**Object Components**

+-------------------------+----------------------------------------------------------------------+
| Component               | Description                                                          |
+=========================+======================================================================+
| List                    |            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Form                    |            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Dashboard               |            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Filter bar              |            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+
| Options                 |            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |
+-------------------------+----------------------------------------------------------------------+

