***************
Browser Support
***************

Browser support varies for each version of the user interface (UI). Most major browsers are supported.

Some features have additional browser requirements, which are noted in the appropriate documentation.

+-------------------------+----------------------------------------------------------------------+
| Browser                 | release                                                              |
+=========================+======================================================================+
| Chrome                  | Latest public release of the browser, and the two previous releases  |
+-------------------------+----------------------------------------------------------------------+
| Firefox                 | Latest public release of the browser, and the two previous releases  |
+-------------------------+----------------------------------------------------------------------+
| Internet Explorer       | Not supported                                                        |
+-------------------------+----------------------------------------------------------------------+
| Microsoft Edge Chromium | Latest public release of the browser, and the two previous releases  |
+-------------------------+----------------------------------------------------------------------+
| Safari                  | Safari 12.0 and up                                                   |
+-------------------------+----------------------------------------------------------------------+

**Internet Explorer**

.. Warning:: Due to significant performance issues, Ignimotion recommends that customers migrate away from Internet Explorer 11.

 * Internet Explorer 11 is susceptible to memory leaks, which may impact performance, especially in Windows 7.
 * Internet Explorer versions prior to IE11 are no longer supported.
 * Compatibility mode is not supported.
 * Setting the Security Mode to High (in the Internet Options > Security tab) is not supported.

