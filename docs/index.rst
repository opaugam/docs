.. include:: ../README.rst

.. toctree::
   :caption: Platform for Contributors
   :maxdepth: 1

   user_interface/ignimotion-overview
   user_interface/navigation-and-ui
   user_interface/common-ui-elements
   user_interface/browser-support

.. toctree::
   :caption: Platform for Designers
   :maxdepth: 1

   features/index
   features/portal/index
   features/reporting/index
   features/data-collection/index
   features/localization/index

.. toctree::
    :maxdepth: 2
    :caption: Platform for Administrators

    administration/index

.. toctree::
    :maxdepth: 2
    :caption: Platform for Developers

    developers/index

.. toctree::
    :maxdepth: 2
    :caption: Release Notes

    release/home-release
