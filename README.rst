********************
Ignimotion Platform
********************
.. sidebar:: More Sites

    .. image:: /img/logo.png

    * `Ignimotion Community <https://forum.ignimotion.com/>`_
    * Best practices
    * `Ignimotion PROTEC <https://docs-protec.ignimotion.com/>`_
    * `Ignimotion.com <https://www.ignimotion.com/>`_

Ignimotion make the last mile of data collection faster, smarter and safer.

**How Ignimotion Platform matches your role**

How you use Ignimotion Platform may depend on your role in a project or on a team. Other people, in other roles, might use Ignimotion Platform differently.

For example, you might primarily use the Ignimotion Platform to view reports and dashboards. Your number-crunching, business-report-creating coworker might make extensive use of Ignimotion Platform to create reports, then publish those reports to the Ignimotion Platform, where you view them. Another coworker, in sales, might mainly use their Ignimotion Platform to monitor progress on sales quotas, and to drill into new sales lead details.

If you're a developer, you might use Ignimotion Platform APIs to push or pull data into datasets or to embed dashboards and reports from external applications (Power BI, Tableau, ...). Have an idea for a new visual? Build it yourself and share it with others.

You also might use each element of Ignimotion Platform at different times, depending on what you're trying to achieve or your role for a given project.

How you use Ignimotion Platform can be based on which feature or service of Ignimotion Platform is the best tool for your situation. For example, you can use Ignimotion Platform to create reports for your own team about customer engagement statistics and you can view inventory and manufacturing progress in a real-time dashboard in the Ignimotion Platform. Each part of Ignimotion Platform is available to you, which is why it's so flexible and compelling.

Explore documents that pertain to your role:
